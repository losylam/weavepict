#include "ofApp.h"

ofImage img;

ofParameter<bool> record;

ofParameter<int> resx;
ofParameter<int> resy;
ofParameter<int> angle;

ofParameter<bool> anim;
ofParameter<float> pos;
ofParameter<bool> mode_audio;
ofParameter<float> volume;

int pos_record;
int max_pos_record;

ofImage grab;

ofDirectory dir;
int cur_img;

//--------------------------------------------------------------
void ofApp::setup(){
  ofSetCircleResolution(100);

  pos_record = 0;
  max_pos_record = 220;

  parameters.setName("knitpict");
  parameters.add(mode_gui.set("mode gui", true));
  parameters.add(mode_audio.set("mode audio", false));
  parameters.add(volume.set("volums", 0.1, 0, 1.0));
  parameters.add(record.set("record", false));
  parameters.add(resx.set("resx", 10, 1, 100));
  parameters.add(resy.set("resy", 10, 1, 100));
  parameters.add(angle.set("angle", 0, 0, 90));
  parameters.add(anim.set("anim", true));
  parameters.add(pos.set("pos", 0, 0, PI));

  parameters.add(fps.set("fps", 0, 0, 100.0));

  gui.setup(parameters);

  cur_img = 0;

  dir.listDir("images");
  dir.allowExt("png");
  dir.sort();

  ofLogNotice(dir.getPath(0));

  //img.load("mire.png");
  img.load(dir.getPath(cur_img));
  //img.load("wax.jpg");

  // ofSetLogLevel(OF_LOG_VERBOSE);

  sync.setup((ofParameterGroup&)gui.getParameter(),6669,"localhost",6667);
  sender.setup("localhost", 7777);


  int sampleRate = 44100;
  int bufferSize = 1024;
  soundStream.setup(this, 2, 0, sampleRate, bufferSize, 4);	

}

//--------------------------------------------------------------
void ofApp::update(){
  ofLogVerbose("update");
  fps = ofGetFrameRate();

  if (mode_audio){
    grab.grabScreen(0, 0, 1024, 1024);
  }
}

//--------------------------------------------------------------
void ofApp::draw(){

  //  img.draw(0,0);

  float pos_x = ofMap(mouseX, 0, ofGetWidth(), 0, 1);
  float pos_y = ofMap(mouseY, 0, ofGetHeight(), 0, 1);

  if (anim){
    pos = ofGetElapsedTimef()/5;
  }

  int stepx = 1024/resx;
  int stepy = 1024/resy;

  int maxx = ofGetWidth()/stepx +1;
  int maxy = ofGetHeight()/stepy +1;

  ofPushMatrix();
  ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
  ofRotate(angle);
  ofTranslate(-ofGetWidth()/2, -ofGetHeight()/2);

  float deci = 0.0;

 
  deci = 1+cos(pos+PI);


  if (record){
    if (pos_record < max_pos_record/2){
      deci = ofxeasing::map(pos_record, 0, max_pos_record/2, 0, 1.8, ofxeasing::cubic::easeInOut);
    }else{
      deci = ofxeasing::map(pos_record, max_pos_record/2, max_pos_record, 1.8, 0, ofxeasing::cubic::easeInOut);
    }
  }

  int hh = img.getHeight();
  int ww = img.getWidth();
  // int hh = 1000;
  // int ww = 1000;

    for (int i = -maxx/2; i < maxx*1.5; i++){
      for (int j = -maxx/2; j < maxy*1.5; j++){
        if (i%2 == 0 && j%2 == 0){
          img.drawSubsection(i*stepx, j*stepy, stepx, stepy, int(i*stepx + j*deci*stepx)%ww, j*stepy);
        }else if (i%2 == 0){
          img.drawSubsection(i*stepx, j*stepy, stepx, stepy, i*stepx, int((j + i*deci)*stepy)%hh);
        }else if(j%2 == 0){
            img.drawSubsection(i*stepx, j*stepy, stepx, stepy, int((i+j*deci)*stepx)%ww, int((j+i*deci)*stepy)%hh);
        }else{
            img.drawSubsection(i*stepx, j*stepy, stepx, stepy, int((i + j*2*deci)*stepx)%ww, int((j+2*i*deci)*stepy)%hh);
        }
      }
    }

  ofPopMatrix();

  if (record){

    // ofPixels pix;
    // fbo_tex.readToPixels(pix);

    char buffer[256];
    sprintf(buffer, "%03d", int(pos_record));
    
    ofSaveScreen("capture_"+ofToString(buffer) + ".png");

    pos_record += 1;

    if (pos_record > max_pos_record){
      pos_record = 0;
      record = false;
    }
  }


  if (mode_gui) gui.draw();
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch(key){
  case OF_KEY_RIGHT:
    cur_img = (cur_img+1)%(int)dir.size();
    ofLogNotice(dir.getPath(cur_img));
    img.load(dir.getPath(cur_img));
    break;

  case OF_KEY_LEFT:
    cur_img = (cur_img-1);
    if (cur_img < 0){
      cur_img = (int)dir.size()-1;
    }
    ofLogNotice(dir.getPath(cur_img));
    img.load(dir.getPath(cur_img));
    break;

  case 'g':
    mode_gui = !mode_gui;
    break;
  case 'f':
    ofToggleFullscreen();
    break;
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

void ofApp::audioOut(ofSoundBuffer & buffer){
  if (mode_audio){



     for (int i = 0; i < buffer.getNumFrames()/2; i++){
       ofColor grab_color_x = grab.getColor(i*2, 1024/2);
       ofColor grab_color_y = grab.getColor(1024/2, i*2);

       buffer[i*buffer.getNumChannels()    ] = ofMap(grab_color_x.r, 0, 255, -1, 1)*volume;
       buffer[i*buffer.getNumChannels() + 1] = ofMap(grab_color_y.b, 0, 255, -1, 1)*volume;
       buffer[(512+i)*buffer.getNumChannels()    ] = ofMap(grab_color_x.r, 0, 255, -1, 1)*volume;
       buffer[(512+i)*buffer.getNumChannels() + 1] = ofMap(grab_color_y.b, 0, 255, -1, 1)*volume;
     }
    // for (int i = 0; i < buffer.getNumFrames(); i++){
    //   if (i < 256){
    //     buffer[i*buffer.getNumChannels()    ] = 0.5;
    //     buffer[i*buffer.getNumChannels() + 1] = 0.5;
    //   }else{
    //     buffer[i*buffer.getNumChannels()    ] = 0.0;
    //     buffer[i*buffer.getNumChannels() + 1] = 0.0;
        
    //   }
    // }
  }
}

